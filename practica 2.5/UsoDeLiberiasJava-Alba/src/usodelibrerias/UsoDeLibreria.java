package usodelibrerias;

import java.util.Scanner;

import librerias.MetodosDeInt;
import librerias.MetodosDeString;

public class UsoDeLibreria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		System.out.println("Metodos de Int");
		
		System.out.println(MetodosDeInt.sumar(16, 34));
		System.out.println(MetodosDeInt.restar(32, 11));
		System.out.println(MetodosDeInt.multiplicar(5, 34));
		System.out.println(MetodosDeInt.dividir(64, 8));
		System.out.println(MetodosDeInt.resto(25, 5));
		
		System.out.println("Metodos de String");
		
		String cadena = "cerilla";
		String cadena2 = "Hola pedro";
		String cadena3 = "Hoy el dia esta muy nublado";
		String cadena4 = "Aerodinamico";
		String cadena5 = "Agua clara";
		
		MetodosDeString.cadenaInversa(cadena);
		System.out.println();
		MetodosDeString.Contiene(cadena2, input);
		System.out.println();
		MetodosDeString.fecha(cadena3, input);
		System.out.println();
		MetodosDeString.mostrarCaracteres(cadena4);
		System.out.println();
		MetodosDeString.sonIguales(cadena5, cadena2, input);
	}
}
	