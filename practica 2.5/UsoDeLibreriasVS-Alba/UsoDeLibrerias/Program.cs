﻿using LibreriaVS_Alba;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsoDeLibrerias
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Metodos de Int");

            Console.WriteLine("Suma");
            Console.WriteLine(MetodosDeInt.sumar(2,9));

            Console.WriteLine("Resta");
            Console.WriteLine(MetodosDeInt.restar(4,12));

            Console.WriteLine("Multiplicacion");
            Console.WriteLine(MetodosDeInt.multiplicar(8,4));

            Console.WriteLine("Divion");
            Console.WriteLine(MetodosDeInt.dividir(24,3));

            Console.WriteLine("Resto");
            Console.WriteLine(MetodosDeInt.resto(50, 10));

            Console.WriteLine("Metodos de String");

            String cadena = "platino";
            String cadena2 = "azulejo";
            String cadena3 = "fecha";
            String cadena4 = "vulcano";
            String cadena5 = "pluton";

            Console.WriteLine("Mostrar caracteres");
            MetodosDeString.mostrarCaracteres(cadena);

            Console.WriteLine();

            Console.WriteLine("Cadenas iguales");
            MetodosDeString.sonIguales(cadena, cadena2);

            Console.WriteLine();

            Console.WriteLine("Cadena inversa");
            MetodosDeString.cadenaInversa(cadena4);

            Console.WriteLine();

            Console.WriteLine("Fecha");
            MetodosDeString.fecha(cadena3);

            Console.WriteLine();

            Console.WriteLine("Contiene?");
            MetodosDeString.Contiene(cadena5);

            Console.WriteLine("Pulsa una tecla para terminar");
            Console.ReadKey();



            
        }
    }
}
