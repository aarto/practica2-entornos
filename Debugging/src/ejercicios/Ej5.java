package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		/*
		 * Este ejercicio nos dice si al introducirle un numero es primo o no 
		 * la i evalua todos los numeros comprendidos entre el 1 y el que hemos metido, y entonces la cantidad de divisores
		 * aumenta solo cuando comprueba que el numero es divisible para 1 y para si mismo 
		 * 
		 * Es decir un numero Primo es aquel que solo tiene 2 divisores, el 1 y el mismo, con l debugger esto lo podemos 
		 * ver claramente
		 * 
		 */
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
