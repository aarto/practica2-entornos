﻿namespace vs_albaarto
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cortarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.insertarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x900ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x180ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x1260ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x1440ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x1800ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x1980ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x2520ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.x2520ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cortarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editarAudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compartirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.correoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hotmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.terraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whatsappToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facebookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instagramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.youtubeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subirArchivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emisionEnDirectoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasoAPasoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label1 = new System.Windows.Forms.Label();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.button49 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.editarToolStripMenuItem,
            this.insertarToolStripMenuItem,
            this.compartirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(964, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.guardarToolStripMenuItem,
            this.guardarComoToolStripMenuItem,
            this.enviarAToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(71, 24);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.carpeta;
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.abrirToolStripMenuItem.Text = "Abrir";
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.guardar;
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.guardarToolStripMenuItem.Text = "Guardar";
            // 
            // guardarComoToolStripMenuItem
            // 
            this.guardarComoToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.guardar;
            this.guardarComoToolStripMenuItem.Name = "guardarComoToolStripMenuItem";
            this.guardarComoToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.guardarComoToolStripMenuItem.Text = "Guardar como...";
            // 
            // enviarAToolStripMenuItem
            // 
            this.enviarAToolStripMenuItem.Name = "enviarAToolStripMenuItem";
            this.enviarAToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.enviarAToolStripMenuItem.Text = "Enviar a...";
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarToolStripMenuItem1,
            this.pegarToolStripMenuItem1,
            this.cortarToolStripMenuItem1,
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem1,
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem1,
            this.abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1});
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.editarToolStripMenuItem.Text = "Editar";
            // 
            // copiarToolStripMenuItem1
            // 
            this.copiarToolStripMenuItem1.Image = global::vs_albaarto.Properties.Resources.copiar;
            this.copiarToolStripMenuItem1.Name = "copiarToolStripMenuItem1";
            this.copiarToolStripMenuItem1.Size = new System.Drawing.Size(387, 26);
            this.copiarToolStripMenuItem1.Text = "Copiar";
            // 
            // pegarToolStripMenuItem1
            // 
            this.pegarToolStripMenuItem1.Image = global::vs_albaarto.Properties.Resources.pegar;
            this.pegarToolStripMenuItem1.Name = "pegarToolStripMenuItem1";
            this.pegarToolStripMenuItem1.Size = new System.Drawing.Size(387, 26);
            this.pegarToolStripMenuItem1.Text = "Pegar";
            // 
            // cortarToolStripMenuItem1
            // 
            this.cortarToolStripMenuItem1.Image = global::vs_albaarto.Properties.Resources.tijeras;
            this.cortarToolStripMenuItem1.Name = "cortarToolStripMenuItem1";
            this.cortarToolStripMenuItem1.Size = new System.Drawing.Size(387, 26);
            this.cortarToolStripMenuItem1.Text = "Cortar";
            // 
            // abrirPaletaDeColoresAvanzadosToolStripMenuItem1
            // 
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem1.Image = global::vs_albaarto.Properties.Resources.colores;
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem1.Name = "abrirPaletaDeColoresAvanzadosToolStripMenuItem1";
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem1.Size = new System.Drawing.Size(387, 26);
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem1.Text = "Abrir paleta de colores avanzados";
            // 
            // abrirMenuDeHerramientasAvanzadasToolStripMenuItem1
            // 
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem1.Image = global::vs_albaarto.Properties.Resources.herramientas;
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem1.Name = "abrirMenuDeHerramientasAvanzadasToolStripMenuItem1";
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem1.Size = new System.Drawing.Size(387, 26);
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem1.Text = "Abrir menu de herramientas avanzadas";
            // 
            // abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1
            // 
            this.abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1.Name = "abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1";
            this.abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1.Size = new System.Drawing.Size(387, 26);
            this.abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1.Text = "Abrir interfaz de edicion de imagen avanzada";
            // 
            // insertarToolStripMenuItem
            // 
            this.insertarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copiarToolStripMenuItem,
            this.pegarToolStripMenuItem,
            this.cortarToolStripMenuItem,
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem,
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem});
            this.insertarToolStripMenuItem.Name = "insertarToolStripMenuItem";
            this.insertarToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.insertarToolStripMenuItem.Text = "Insertar";
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.x900ToolStripMenuItem,
            this.x180ToolStripMenuItem,
            this.x1260ToolStripMenuItem,
            this.x1440ToolStripMenuItem,
            this.x1800ToolStripMenuItem,
            this.x1980ToolStripMenuItem,
            this.x2520ToolStripMenuItem,
            this.x2520ToolStripMenuItem1});
            this.copiarToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.imagen;
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.copiarToolStripMenuItem.Text = "Imagen";
            this.copiarToolStripMenuItem.Click += new System.EventHandler(this.copiarToolStripMenuItem_Click);
            // 
            // x900ToolStripMenuItem
            // 
            this.x900ToolStripMenuItem.Name = "x900ToolStripMenuItem";
            this.x900ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x900ToolStripMenuItem.Text = "630 x 900";
            // 
            // x180ToolStripMenuItem
            // 
            this.x180ToolStripMenuItem.Name = "x180ToolStripMenuItem";
            this.x180ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x180ToolStripMenuItem.Text = "720 x 180";
            // 
            // x1260ToolStripMenuItem
            // 
            this.x1260ToolStripMenuItem.Name = "x1260ToolStripMenuItem";
            this.x1260ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x1260ToolStripMenuItem.Text = "900 x 1260";
            // 
            // x1440ToolStripMenuItem
            // 
            this.x1440ToolStripMenuItem.Name = "x1440ToolStripMenuItem";
            this.x1440ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x1440ToolStripMenuItem.Text = "1080 x 1440";
            // 
            // x1800ToolStripMenuItem
            // 
            this.x1800ToolStripMenuItem.Name = "x1800ToolStripMenuItem";
            this.x1800ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x1800ToolStripMenuItem.Text = "1440 x 1800";
            // 
            // x1980ToolStripMenuItem
            // 
            this.x1980ToolStripMenuItem.Name = "x1980ToolStripMenuItem";
            this.x1980ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x1980ToolStripMenuItem.Text = "1530 x 1980";
            // 
            // x2520ToolStripMenuItem
            // 
            this.x2520ToolStripMenuItem.Name = "x2520ToolStripMenuItem";
            this.x2520ToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
            this.x2520ToolStripMenuItem.Text = "1800 x 2520";
            // 
            // x2520ToolStripMenuItem1
            // 
            this.x2520ToolStripMenuItem1.Name = "x2520ToolStripMenuItem1";
            this.x2520ToolStripMenuItem1.Size = new System.Drawing.Size(163, 26);
            this.x2520ToolStripMenuItem1.Text = "1980 x 2520";
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.collage;
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.pegarToolStripMenuItem.Text = "Collage";
            // 
            // cortarToolStripMenuItem
            // 
            this.cortarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem1,
            this.editarAudioToolStripMenuItem});
            this.cortarToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.audios;
            this.cortarToolStripMenuItem.Name = "cortarToolStripMenuItem";
            this.cortarToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.cortarToolStripMenuItem.Text = "Audio";
            // 
            // abrirToolStripMenuItem1
            // 
            this.abrirToolStripMenuItem1.Image = global::vs_albaarto.Properties.Resources.carpetaudio;
            this.abrirToolStripMenuItem1.Name = "abrirToolStripMenuItem1";
            this.abrirToolStripMenuItem1.Size = new System.Drawing.Size(165, 26);
            this.abrirToolStripMenuItem1.Text = "Abrir";
            // 
            // editarAudioToolStripMenuItem
            // 
            this.editarAudioToolStripMenuItem.Name = "editarAudioToolStripMenuItem";
            this.editarAudioToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.editarAudioToolStripMenuItem.Text = "Editar audio";
            // 
            // abrirPaletaDeColoresAvanzadosToolStripMenuItem
            // 
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buscarToolStripMenuItem});
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.gif;
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem.Name = "abrirPaletaDeColoresAvanzadosToolStripMenuItem";
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem.Text = "Gif";
            this.abrirPaletaDeColoresAvanzadosToolStripMenuItem.Click += new System.EventHandler(this.abrirPaletaDeColoresAvanzadosToolStripMenuItem_Click);
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.lupa;
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(127, 26);
            this.buscarToolStripMenuItem.Text = "Buscar";
            // 
            // abrirMenuDeHerramientasAvanzadasToolStripMenuItem
            // 
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.emoticono;
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem.Name = "abrirMenuDeHerramientasAvanzadasToolStripMenuItem";
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem.Size = new System.Drawing.Size(162, 26);
            this.abrirMenuDeHerramientasAvanzadasToolStripMenuItem.Text = "Emoticonos";
            // 
            // compartirToolStripMenuItem
            // 
            this.compartirToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.correoToolStripMenuItem,
            this.whatsappToolStripMenuItem,
            this.twitterToolStripMenuItem,
            this.facebookToolStripMenuItem,
            this.instagramToolStripMenuItem,
            this.youtubeToolStripMenuItem});
            this.compartirToolStripMenuItem.Name = "compartirToolStripMenuItem";
            this.compartirToolStripMenuItem.Size = new System.Drawing.Size(88, 24);
            this.compartirToolStripMenuItem.Text = "Compartir";
            // 
            // correoToolStripMenuItem
            // 
            this.correoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gmailToolStripMenuItem,
            this.hotmailToolStripMenuItem,
            this.terraToolStripMenuItem});
            this.correoToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.correo;
            this.correoToolStripMenuItem.Name = "correoToolStripMenuItem";
            this.correoToolStripMenuItem.Size = new System.Drawing.Size(151, 26);
            this.correoToolStripMenuItem.Text = "Correo";
            // 
            // gmailToolStripMenuItem
            // 
            this.gmailToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.gmail;
            this.gmailToolStripMenuItem.Name = "gmailToolStripMenuItem";
            this.gmailToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.gmailToolStripMenuItem.Text = "Gmail";
            // 
            // hotmailToolStripMenuItem
            // 
            this.hotmailToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.hotmail;
            this.hotmailToolStripMenuItem.Name = "hotmailToolStripMenuItem";
            this.hotmailToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.hotmailToolStripMenuItem.Text = "Hotmail";
            // 
            // terraToolStripMenuItem
            // 
            this.terraToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.terra;
            this.terraToolStripMenuItem.Name = "terraToolStripMenuItem";
            this.terraToolStripMenuItem.Size = new System.Drawing.Size(138, 26);
            this.terraToolStripMenuItem.Text = "Terra";
            // 
            // whatsappToolStripMenuItem
            // 
            this.whatsappToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.waspas;
            this.whatsappToolStripMenuItem.Name = "whatsappToolStripMenuItem";
            this.whatsappToolStripMenuItem.Size = new System.Drawing.Size(151, 26);
            this.whatsappToolStripMenuItem.Text = "Whatsapp";
            // 
            // twitterToolStripMenuItem
            // 
            this.twitterToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.twitter;
            this.twitterToolStripMenuItem.Name = "twitterToolStripMenuItem";
            this.twitterToolStripMenuItem.Size = new System.Drawing.Size(151, 26);
            this.twitterToolStripMenuItem.Text = "Twitter";
            // 
            // facebookToolStripMenuItem
            // 
            this.facebookToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.facebook;
            this.facebookToolStripMenuItem.Name = "facebookToolStripMenuItem";
            this.facebookToolStripMenuItem.Size = new System.Drawing.Size(151, 26);
            this.facebookToolStripMenuItem.Text = "Facebook";
            // 
            // instagramToolStripMenuItem
            // 
            this.instagramToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.instagram;
            this.instagramToolStripMenuItem.Name = "instagramToolStripMenuItem";
            this.instagramToolStripMenuItem.Size = new System.Drawing.Size(151, 26);
            this.instagramToolStripMenuItem.Text = "Instagram";
            // 
            // youtubeToolStripMenuItem
            // 
            this.youtubeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subirArchivoToolStripMenuItem,
            this.emisionEnDirectoToolStripMenuItem,
            this.pasoAPasoToolStripMenuItem});
            this.youtubeToolStripMenuItem.Image = global::vs_albaarto.Properties.Resources.youtube;
            this.youtubeToolStripMenuItem.Name = "youtubeToolStripMenuItem";
            this.youtubeToolStripMenuItem.Size = new System.Drawing.Size(151, 26);
            this.youtubeToolStripMenuItem.Text = "Youtube";
            // 
            // subirArchivoToolStripMenuItem
            // 
            this.subirArchivoToolStripMenuItem.Name = "subirArchivoToolStripMenuItem";
            this.subirArchivoToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.subirArchivoToolStripMenuItem.Text = "Subir archivo";
            // 
            // emisionEnDirectoToolStripMenuItem
            // 
            this.emisionEnDirectoToolStripMenuItem.Name = "emisionEnDirectoToolStripMenuItem";
            this.emisionEnDirectoToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.emisionEnDirectoToolStripMenuItem.Text = "Emision en directo";
            // 
            // pasoAPasoToolStripMenuItem
            // 
            this.pasoAPasoToolStripMenuItem.Name = "pasoAPasoToolStripMenuItem";
            this.pasoAPasoToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.pasoAPasoToolStripMenuItem.Text = "Paso a paso";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::vs_albaarto.Properties.Resources.woman_eyes_ornament_pencil_drawing_eye_contact_crackle_effect_computer_collage_70297117;
            this.pictureBox1.Location = new System.Drawing.Point(314, 60);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(405, 288);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(38, 95);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(19, 15);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(163, 95);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(19, 15);
            this.button2.TabIndex = 4;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button3.Location = new System.Drawing.Point(138, 95);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(19, 15);
            this.button3.TabIndex = 5;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gray;
            this.button4.Location = new System.Drawing.Point(113, 95);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(19, 15);
            this.button4.TabIndex = 6;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Silver;
            this.button5.Location = new System.Drawing.Point(88, 95);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(19, 15);
            this.button5.TabIndex = 7;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.button6.Location = new System.Drawing.Point(63, 95);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(19, 15);
            this.button6.TabIndex = 8;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button7.Location = new System.Drawing.Point(38, 116);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(19, 15);
            this.button7.TabIndex = 9;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button8.Location = new System.Drawing.Point(63, 116);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(19, 15);
            this.button8.TabIndex = 10;
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Red;
            this.button9.Location = new System.Drawing.Point(88, 116);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(19, 15);
            this.button9.TabIndex = 11;
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button10.Location = new System.Drawing.Point(113, 116);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(19, 15);
            this.button10.TabIndex = 12;
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Maroon;
            this.button11.Location = new System.Drawing.Point(138, 116);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(19, 15);
            this.button11.TabIndex = 13;
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button12.Location = new System.Drawing.Point(163, 116);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(19, 15);
            this.button12.TabIndex = 14;
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button13.Location = new System.Drawing.Point(63, 137);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(19, 15);
            this.button13.TabIndex = 15;
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button14.Location = new System.Drawing.Point(38, 137);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(19, 15);
            this.button14.TabIndex = 16;
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button15.Location = new System.Drawing.Point(88, 137);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(19, 15);
            this.button15.TabIndex = 17;
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button16.Location = new System.Drawing.Point(138, 137);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(19, 15);
            this.button16.TabIndex = 18;
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button17.Location = new System.Drawing.Point(113, 137);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(19, 15);
            this.button17.TabIndex = 19;
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button18.Location = new System.Drawing.Point(163, 137);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(19, 15);
            this.button18.TabIndex = 20;
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button19.Location = new System.Drawing.Point(38, 158);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(19, 15);
            this.button19.TabIndex = 21;
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button20.Location = new System.Drawing.Point(63, 158);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(19, 15);
            this.button20.TabIndex = 22;
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.Yellow;
            this.button21.Location = new System.Drawing.Point(88, 158);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(19, 15);
            this.button21.TabIndex = 23;
            this.button21.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.Olive;
            this.button22.Location = new System.Drawing.Point(138, 158);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(19, 15);
            this.button22.TabIndex = 24;
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button23.Location = new System.Drawing.Point(113, 158);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(19, 15);
            this.button23.TabIndex = 25;
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button24.Location = new System.Drawing.Point(163, 158);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(19, 15);
            this.button24.TabIndex = 26;
            this.button24.UseVisualStyleBackColor = false;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(18, 298);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 27;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Calendario de entregas ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button25.Location = new System.Drawing.Point(38, 179);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(19, 15);
            this.button25.TabIndex = 29;
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button26.Location = new System.Drawing.Point(63, 179);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(19, 15);
            this.button26.TabIndex = 30;
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.Lime;
            this.button27.Location = new System.Drawing.Point(88, 179);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(19, 15);
            this.button27.TabIndex = 31;
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.Green;
            this.button28.Location = new System.Drawing.Point(138, 179);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(19, 15);
            this.button28.TabIndex = 32;
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button29.Location = new System.Drawing.Point(113, 179);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(19, 15);
            this.button29.TabIndex = 33;
            this.button29.UseVisualStyleBackColor = false;
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button30.Location = new System.Drawing.Point(163, 179);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(19, 15);
            this.button30.TabIndex = 34;
            this.button30.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button31.Location = new System.Drawing.Point(63, 200);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(19, 15);
            this.button31.TabIndex = 35;
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button32.Location = new System.Drawing.Point(38, 200);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(19, 15);
            this.button32.TabIndex = 36;
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.button33.Location = new System.Drawing.Point(113, 200);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(19, 15);
            this.button33.TabIndex = 37;
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.Aqua;
            this.button34.Location = new System.Drawing.Point(88, 200);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(19, 15);
            this.button34.TabIndex = 38;
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.Teal;
            this.button35.Location = new System.Drawing.Point(138, 200);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(19, 15);
            this.button35.TabIndex = 39;
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button36.Location = new System.Drawing.Point(163, 200);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(19, 15);
            this.button36.TabIndex = 40;
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button37.Location = new System.Drawing.Point(38, 221);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(19, 15);
            this.button37.TabIndex = 41;
            this.button37.UseVisualStyleBackColor = false;
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button38.Location = new System.Drawing.Point(63, 221);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(19, 15);
            this.button38.TabIndex = 42;
            this.button38.UseVisualStyleBackColor = false;
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.Blue;
            this.button39.Location = new System.Drawing.Point(88, 221);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(19, 15);
            this.button39.TabIndex = 43;
            this.button39.UseVisualStyleBackColor = false;
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button40.Location = new System.Drawing.Point(113, 221);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(19, 15);
            this.button40.TabIndex = 44;
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button41.Location = new System.Drawing.Point(163, 221);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(19, 15);
            this.button41.TabIndex = 45;
            this.button41.UseVisualStyleBackColor = false;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.Navy;
            this.button42.Location = new System.Drawing.Point(138, 221);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(19, 15);
            this.button42.TabIndex = 46;
            this.button42.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button43.Location = new System.Drawing.Point(38, 242);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(19, 15);
            this.button43.TabIndex = 47;
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button44.Location = new System.Drawing.Point(63, 242);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(19, 15);
            this.button44.TabIndex = 48;
            this.button44.UseVisualStyleBackColor = false;
            // 
            // button45
            // 
            this.button45.BackColor = System.Drawing.Color.Fuchsia;
            this.button45.Location = new System.Drawing.Point(88, 242);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(19, 15);
            this.button45.TabIndex = 49;
            this.button45.UseVisualStyleBackColor = false;
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.button46.Location = new System.Drawing.Point(113, 242);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(19, 15);
            this.button46.TabIndex = 50;
            this.button46.UseVisualStyleBackColor = false;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.button47.Location = new System.Drawing.Point(163, 242);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(19, 15);
            this.button47.TabIndex = 51;
            this.button47.UseVisualStyleBackColor = false;
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.Color.Purple;
            this.button48.Location = new System.Drawing.Point(138, 242);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(19, 15);
            this.button48.TabIndex = 52;
            this.button48.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 17);
            this.label2.TabIndex = 53;
            this.label2.Text = "Paleta de colores ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(659, 373);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(46, 22);
            this.numericUpDown1.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(322, 375);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 17);
            this.label3.TabIndex = 55;
            this.label3.Text = "Resolucion del pixel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(322, 422);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 56;
            this.label4.Text = "Zoom";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(737, 89);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(193, 21);
            this.progressBar1.TabIndex = 57;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(734, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(209, 17);
            this.label5.TabIndex = 58;
            this.label5.Text = "Progreso de escaneo del dibujo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(791, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 17);
            this.label6.TabIndex = 60;
            this.label6.Text = "Herramientas";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Pincel Redondo",
            "Pincel Plano",
            "Pincel Fino",
            "Pincel Extrafino",
            "Pincel Medio"});
            this.comboBox1.Location = new System.Drawing.Point(761, 175);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(169, 24);
            this.comboBox1.TabIndex = 61;
            this.comboBox1.Text = "Pinceles";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(659, 417);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(46, 22);
            this.numericUpDown2.TabIndex = 62;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Pluma Extrafina",
            "Pluma Fina",
            "Pluma Media",
            "Pluma Gruesa",
            "Pluma de punta Redonda",
            "Pluma de doble Hoja"});
            this.comboBox2.Location = new System.Drawing.Point(761, 221);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(169, 24);
            this.comboBox2.TabIndex = 63;
            this.comboBox2.Text = "Plumas";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Lapiz Grueso",
            "Lapiz Medio",
            "Lapiz fino ",
            "Carboncillo de Vid",
            "Carboncillo de Sauce",
            "Carbon comprimido",
            "Barra de grafito"});
            this.comboBox3.Location = new System.Drawing.Point(761, 265);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(169, 24);
            this.comboBox3.TabIndex = 64;
            this.comboBox3.Text = "Lapiz y carboncillos";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(791, 353);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 17);
            this.label7.TabIndex = 65;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(761, 329);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(113, 21);
            this.radioButton1.TabIndex = 66;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Activar Guias";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(809, 475);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(134, 30);
            this.button49.TabIndex = 67;
            this.button49.Text = "Guardado Rapido";
            this.button49.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 523);
            this.Controls.Add(this.button49);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button48);
            this.Controls.Add(this.button47);
            this.Controls.Add(this.button46);
            this.Controls.Add(this.button45);
            this.Controls.Add(this.button44);
            this.Controls.Add(this.button43);
            this.Controls.Add(this.button42);
            this.Controls.Add(this.button41);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.button39);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Paint Tool Flor de Lis";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compartirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirPaletaDeColoresAvanzadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirMenuDeHerramientasAvanzadasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cortarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrirPaletaDeColoresAvanzadosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrirMenuDeHerramientasAvanzadasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrirInterfazDeEdicionDeImagenAvanzadaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cortarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x900ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x180ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x1260ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x1440ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x1800ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x1980ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x2520ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem x2520ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editarAudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem correoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hotmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem terraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whatsappToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem twitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facebookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instagramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem youtubeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subirArchivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emisionEnDirectoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasoAPasoToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button button49;
    }
}

