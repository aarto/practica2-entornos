package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JCheckBox;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaPrincipal extends JFrame {
	private JTextField textField;

	/**
	 * @author Alba Arto
	 * @since 21/12/1017
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setTitle("Paint Tool Flor de Lis\r\n");
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/imagenes/marco-de-plata-de-la-estrella-con-un-icono-de-la-flor-de-lis-12003163.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 744, 461);
		getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 726, 26);
		getContentPane().add(menuBar);
		
		JMenu mnAbrir = new JMenu("Archivo");
		menuBar.add(mnAbrir);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Abrir");
		mntmNewMenuItem.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/carpeta.png")));
		mnAbrir.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Guardar");
		mntmNewMenuItem_1.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/guardar.png")));
		mnAbrir.add(mntmNewMenuItem_1);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como...");
		mntmGuardarComo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/guardar.png")));
		mnAbrir.add(mntmGuardarComo);
		
		JMenuItem mntmEnviarA = new JMenuItem("Enviar a...");
		mnAbrir.add(mntmEnviarA);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mntmCopiar.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/copiar.jpg")));
		mnEditar.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mntmPegar.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/pegar.png")));
		mnEditar.add(mntmPegar);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mntmCortar.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/tijeras.jpg")));
		mnEditar.add(mntmCortar);
		
		JMenuItem mntmAbrirPaletaDe = new JMenuItem("Abrir paleta de colores avanzados");
		mntmAbrirPaletaDe.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/colores.png")));
		mnEditar.add(mntmAbrirPaletaDe);
		
		JMenuItem mntmAbrirMenuDe = new JMenuItem("Abrir menu de herramientas avanzadas");
		mntmAbrirMenuDe.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/herramientas.png")));
		mnEditar.add(mntmAbrirMenuDe);
		
		JMenuItem mntmAbrirInterfazDe = new JMenuItem("Abrir interfaz de edicion de imagen avanzada");
		mnEditar.add(mntmAbrirInterfazDe);
		
		JMenu mnInsertar = new JMenu("Insertar");
		menuBar.add(mnInsertar);
		
		JMenuItem mntmImagen = new JMenuItem("Imagen");
		mntmImagen.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/imagen.png")));
		mnInsertar.add(mntmImagen);
		
		JMenuItem mntmCollage = new JMenuItem("Collage");
		mntmCollage.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/collage.jpg")));
		mnInsertar.add(mntmCollage);
		
		JMenuItem mntmAudio = new JMenuItem("Audio");
		mntmAudio.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/audios.jpg")));
		mnInsertar.add(mntmAudio);
		
		JMenuItem mntmGif = new JMenuItem("Gif");
		mntmGif.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/gif.png")));
		mnInsertar.add(mntmGif);
		
		JMenuItem mntmEmoticonos = new JMenuItem("Emoticonos");
		mntmEmoticonos.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/emoticono.jpg")));
		mnInsertar.add(mntmEmoticonos);
		
		JMenu mnCompartir = new JMenu("Compartir");
		menuBar.add(mnCompartir);
		
		JMenuItem mntmCorreo = new JMenuItem("Correo");
		mntmCorreo.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/correo.png")));
		mnCompartir.add(mntmCorreo);
		
		JMenuItem mntmWhatsapp = new JMenuItem("Whatsapp");
		mntmWhatsapp.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/waspas.png")));
		mnCompartir.add(mntmWhatsapp);
		
		JMenuItem mntmTwitter = new JMenuItem("Twitter");
		mntmTwitter.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/twitter.png")));
		mnCompartir.add(mntmTwitter);
		
		JMenuItem mntmFacebook = new JMenuItem("Facebook");
		mntmFacebook.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/facebook.png")));
		mnCompartir.add(mntmFacebook);
		
		JMenuItem mntmInstragram = new JMenuItem("Instragram");
		mntmInstragram.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/instagram.png")));
		mnCompartir.add(mntmInstragram);
		
		JMenuItem mntmYoutube = new JMenuItem("Youtube");
		mntmYoutube.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/youtube.png")));
		mnCompartir.add(mntmYoutube);
		
		JSlider slider = new JSlider();
		slider.setBounds(436, 288, 278, 26);
		getContentPane().add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(654, 367, 60, 34);
		getContentPane().add(spinner);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(23, 112, 24, 16);
		getContentPane().add(btnNewButton);
		
		JLabel lblPaletaDeColor = new JLabel("Paleta de colores basica");
		lblPaletaDeColor.setBounds(20, 39, 168, 26);
		getContentPane().add(lblPaletaDeColor);
		
		textField = new JTextField();
		textField.setBackground(Color.RED);
		textField.setBounds(20, 68, 168, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton button = new JButton("");
		button.setBackground(Color.LIGHT_GRAY);
		button.setBounds(56, 112, 24, 16);
		getContentPane().add(button);
		
		JButton button_1 = new JButton("");
		button_1.setBackground(Color.GRAY);
		button_1.setBounds(92, 112, 24, 16);
		getContentPane().add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.setBackground(Color.DARK_GRAY);
		button_2.setBounds(128, 112, 24, 16);
		getContentPane().add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.setBackground(Color.BLACK);
		button_3.setBounds(164, 112, 24, 16);
		getContentPane().add(button_3);
		
		JButton button_4 = new JButton("");
		button_4.setBackground(new Color(255, 0, 0));
		button_4.setBounds(23, 141, 24, 16);
		getContentPane().add(button_4);
		
		JButton button_5 = new JButton("");
		button_5.setBackground(new Color(255, 69, 0));
		button_5.setBounds(56, 141, 24, 16);
		getContentPane().add(button_5);
		
		JButton button_6 = new JButton("");
		button_6.setBackground(new Color(255, 140, 0));
		button_6.setBounds(92, 141, 24, 16);
		getContentPane().add(button_6);
		
		JButton button_7 = new JButton("");
		button_7.setBackground(new Color(255, 165, 0));
		button_7.setBounds(128, 141, 24, 16);
		getContentPane().add(button_7);
		
		JButton button_8 = new JButton("");
		button_8.setBackground(new Color(255, 255, 0));
		button_8.setBounds(164, 141, 24, 16);
		getContentPane().add(button_8);
		
		JButton button_9 = new JButton("");
		button_9.setBackground(new Color(0, 0, 205));
		button_9.setBounds(23, 170, 24, 16);
		getContentPane().add(button_9);
		
		JButton button_10 = new JButton("");
		button_10.setBackground(new Color(0, 0, 255));
		button_10.setBounds(56, 170, 24, 16);
		getContentPane().add(button_10);
		
		JButton button_11 = new JButton("");
		button_11.setBackground(new Color(30, 144, 255));
		button_11.setBounds(92, 170, 24, 16);
		getContentPane().add(button_11);
		
		JButton button_12 = new JButton("");
		button_12.setBackground(new Color(0, 206, 209));
		button_12.setBounds(128, 170, 24, 16);
		getContentPane().add(button_12);
		
		JButton button_13 = new JButton("");
		button_13.setBackground(new Color(0, 255, 255));
		button_13.setBounds(164, 170, 24, 16);
		getContentPane().add(button_13);
		
		JButton button_14 = new JButton("");
		button_14.setBackground(new Color(128, 0, 128));
		button_14.setBounds(23, 199, 24, 16);
		getContentPane().add(button_14);
		
		JButton button_15 = new JButton("");
		button_15.setBackground(new Color(148, 0, 211));
		button_15.setBounds(56, 199, 24, 16);
		getContentPane().add(button_15);
		
		JButton button_16 = new JButton("");
		button_16.setBackground(new Color(147, 112, 219));
		button_16.setBounds(92, 199, 24, 16);
		getContentPane().add(button_16);
		
		JButton button_17 = new JButton("");
		button_17.setBackground(new Color(199, 21, 133));
		button_17.setBounds(128, 199, 24, 16);
		getContentPane().add(button_17);
		
		JButton button_18 = new JButton("");
		button_18.setBackground(new Color(255, 0, 255));
		button_18.setBounds(164, 199, 24, 16);
		getContentPane().add(button_18);
		
		JButton button_19 = new JButton("");
		button_19.setBackground(new Color(0, 128, 0));
		button_19.setBounds(23, 228, 24, 16);
		getContentPane().add(button_19);
		
		JButton button_20 = new JButton("");
		button_20.setBackground(new Color(0, 128, 0));
		button_20.setBounds(56, 228, 24, 16);
		getContentPane().add(button_20);
		
		JButton button_21 = new JButton("");
		button_21.setBackground(new Color(50, 205, 50));
		button_21.setBounds(92, 228, 24, 16);
		getContentPane().add(button_21);
		
		JButton button_22 = new JButton("");
		button_22.setBackground(new Color(0, 255, 0));
		button_22.setBounds(128, 228, 24, 16);
		getContentPane().add(button_22);
		
		JButton button_23 = new JButton("");
		button_23.setBackground(new Color(124, 252, 0));
		button_23.setBounds(164, 228, 24, 16);
		getContentPane().add(button_23);
		
		JLabel lblZoom = new JLabel("Zoom");
		lblZoom.setBounds(356, 288, 68, 16);
		getContentPane().add(lblZoom);
		
		JLabel label = new JLabel("0");
		label.setBounds(435, 308, 24, 26);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("100");
		label_1.setBounds(694, 313, 32, 16);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("50");
		label_2.setBounds(570, 313, 24, 16);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("|");
		label_3.setBounds(486, 313, 56, 16);
		getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("|");
		label_4.setBounds(527, 313, 56, 16);
		getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("|");
		label_5.setBounds(610, 313, 56, 16);
		getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("|");
		label_6.setBounds(654, 313, 56, 16);
		getContentPane().add(label_6);
		
		JLabel lblClaridadDelPixel = new JLabel("Claridad del pixel");
		lblClaridadDelPixel.setBounds(356, 376, 103, 16);
		getContentPane().add(lblClaridadDelPixel);
		
		JLabel lblHerramientas = new JLabel("Herramientas basicas");
		lblHerramientas.setBounds(20, 275, 132, 16);
		getContentPane().add(lblHerramientas);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Pincel", "Lapiz", "Pluma", "Efecto carboncillo"}));
		comboBox.setBounds(23, 310, 132, 26);
		getContentPane().add(comboBox);
		
		JButton btnGuardadoRapido = new JButton("Guardado rapido");
		btnGuardadoRapido.setBounds(23, 376, 149, 25);
		getContentPane().add(btnGuardadoRapido);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/imagenes/woman-eyes-ornament-pencil-drawing-eye-contact-crackle-effect-computer-collage-70297117.jpg")));
		lblNewLabel.setBounds(307, 44, 407, 230);
		getContentPane().add(lblNewLabel);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
	}
}
